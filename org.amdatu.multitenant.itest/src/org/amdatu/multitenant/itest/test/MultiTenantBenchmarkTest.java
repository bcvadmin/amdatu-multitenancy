/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.multitenant.itest.test;

import static org.amdatu.multitenant.Constants.PID_KEY;
import static org.amdatu.multitenant.itest.test.MultiTenantTest.countServices;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import junit.framework.TestCase;

import org.amdatu.multitenant.Constants;
import org.amdatu.multitenant.TenantFactoryConfiguration;
import org.amdatu.multitenant.itest.MyDependentService;
import org.amdatu.testing.configurator.TestConfigurator;
import org.apache.felix.dm.DependencyManager;
import org.osgi.framework.BundleContext;
import org.osgi.framework.FrameworkUtil;
import org.osgi.framework.ServiceReference;
import org.osgi.service.cm.Configuration;
import org.osgi.service.log.LogService;

/**
 * Benchmark test for multi-tenancy. Tests whether it is possible to run a "large" number of tenants in a single container.
 *
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 */

public class MultiTenantBenchmarkTest extends TestCase {
	private static final int TENANT_COUNT = 100; 
	private volatile DependencyManager m_manager;
    private BundleContext m_bundleContext = FrameworkUtil.getBundle(this.getClass()).getBundleContext();
    private TenantConfigurator m_tenantConfigurator;
    private final List<Configuration> m_configurations = new ArrayList<Configuration>();


    /**
     * Sets up an individual test case.
     *
     * @throws Exception not part of this set up.
     */
    public void setUp() throws Exception {
    	
        TestConfigurator.configure(this)
            .add(TestConfigurator.createServiceDependency().setService(TenantFactoryConfiguration.class).setRequired(true))
            .add(TestConfigurator.createServiceDependency().setService(LogService.class).setRequired(true))
            .apply();
        
        m_tenantConfigurator = new TenantConfigurator();
        m_manager.add(m_manager.createComponent().setImplementation(m_tenantConfigurator)
            .add(m_manager.createServiceDependency().setService(TenantFactoryConfiguration.class).setRequired(true)));
        m_tenantConfigurator.configureTenants(Constants.PID_VALUE_PLATFORM, "Default");
        m_tenantConfigurator.waitForSystemToSettle();
    }

    /**
     * Tears down the test case.
     */
    public void tearDown() {
        for (Configuration config : m_configurations) {
            try {
                config.delete();
            }
            catch (Exception exception) {
                // Ignore...
            }
        }
        m_configurations.clear();
        
        TestConfigurator.cleanUp(this);
    }

    /**
     * Tests that for a single OSGi-container many tenant services can be materialized.
     *
     * @throws Exception not part of this test case.
     */
    public void testBenchmark() throws Exception {
        List<String> createdTenantPIDs = new ArrayList<String>(TENANT_COUNT);

        // Ramp up: create up to TENANT_COUNT tenants (start at one as we get a _PLATFORM tenant for free)...
        String[] tenants = new String[TENANT_COUNT];
        for (int i = 0; i < TENANT_COUNT; i++) {
            tenants[i] = "tenant-" + i;
        }
        
        m_tenantConfigurator.configureTenants(tenants);

        long timeout = TENANT_COUNT * TENANT_COUNT;

        // Wait until the system has been settled and all tenants are activated...
        waitUntilServicesAreRegistered(timeout, TENANT_COUNT, MyDependentService.class);

        assertEquals(TENANT_COUNT, countMyDependentServices());

        List<Configuration> tmpConfigs = new ArrayList<Configuration>(m_configurations);

        // Make a call to each and every tenant...
        List<String> seenList = new ArrayList<String>(createdTenantPIDs);
        
        for (int i = 0; i < TENANT_COUNT; i++) {
            String tenantPID = tenants[i];
            MyDependentService service = getServiceInstance(tenantPID);
            assertNotNull(service.sayIt());

            // Mark this tenant as seen...
            seenList.remove(tenantPID);
        }

        assertEquals("Not all tenants were seen/called?!", 0, seenList.size());

        // We should have seen *no* new tenants coming in, or old tenants leaving us...
        assertEquals(TENANT_COUNT, countMyDependentServices());

        // Ramp down: remove each and every tenant instance...
        m_tenantConfigurator.configureTenants(Constants.PID_VALUE_PLATFORM);

        // Wait until the system has been settled and all tenants are activated...
        waitUntilServicesAreRegistered(timeout, 1, MyDependentService.class);

        // We should have only the platform tenant left by now...
        assertEquals(1, countMyDependentServices());
    }

    /**
     * Waits until either a given number of services is seen, or until a certain timeout occurred.
     *
     * @param timeout the time out, in milliseconds;
     * @param serviceCount the number of services to expect;
     * @param countedServiceClass the service to count.
     * @throws InterruptedException in case we're being interrupted while waiting for the services to come up.
     */
    private void waitUntilServicesAreRegistered(long timeout, int serviceCount, Class<?> countedServiceClass)
        throws InterruptedException {
        long end = System.currentTimeMillis() + timeout;

        int sc;
        do {
            TimeUnit.MILLISECONDS.sleep(10);
            sc = countServices(m_bundleContext, countedServiceClass.getName());
        }
        while ((sc != serviceCount) && (System.currentTimeMillis() < end));
    }

    /**
     * @param tenantPID
     * @return
     */
    private MyDependentService getServiceInstance(String tenantPID) throws Exception {

        ServiceReference[] serviceRefs =
            m_bundleContext.getServiceReferences(MyDependentService.class.getName(),
                String.format("(%1$s=%2$s)", PID_KEY, tenantPID));
        if (serviceRefs == null || serviceRefs.length == 0) {
            return null;
        }

        MyDependentService service = (MyDependentService) m_bundleContext.getService(serviceRefs[0]);
        if (service == null) {
            return null;
        }

        return service;

    }

    /**
     * @return the number of {@link MyDependentService} instances registered with OSGi.
     */
    private int countMyDependentServices() {
        return countServices(m_bundleContext, MyDependentService.class.getName());
    }
}
