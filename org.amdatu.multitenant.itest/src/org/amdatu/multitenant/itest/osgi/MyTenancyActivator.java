/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.multitenant.itest.osgi;

import org.amdatu.multitenant.itest.MyDependencyService;
import org.amdatu.multitenant.itest.MyDependentService;
import org.amdatu.multitenant.itest.MyGlobalService;
import org.amdatu.multitenant.itest.impl.MyDependencyServiceImpl;
import org.amdatu.multitenant.itest.impl.MyDependentServiceImpl;
import org.amdatu.multitenant.itest.impl.MyGlobalServiceImpl;
import org.apache.felix.dm.DependencyActivatorBase;
import org.apache.felix.dm.DependencyManager;
import org.osgi.framework.BundleContext;
import org.osgi.service.log.LogReaderService;
import org.osgi.service.log.LogService;

/**
 * Test activator for {@link MyDependentService}.
 * 
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 */
public class MyTenancyActivator extends DependencyActivatorBase {
    /**
     * @see org.apache.felix.dm.DependencyActivatorBase#init(org.osgi.framework.BundleContext, org.apache.felix.dm.DependencyManager)
     */
    @Override
    public void init(BundleContext context, DependencyManager manager) throws Exception {

        manager.add(createComponent()
            .setInterface(MyGlobalService.class.getName(), null)
            .setImplementation(new MyGlobalServiceImpl())
            .add(createServiceDependency()
                .setService(LogService.class)
                .setRequired(true)));

        manager.add(createComponent()
            .setInterface(MyDependencyService.class.getName(), null)
            .setImplementation(new MyDependencyServiceImpl())
            .add(createServiceDependency()
                .setService(LogService.class)
                .setRequired(true)));

        manager.add(createComponent()
            .setInterface(MyDependentService.class.getName(), null)
            .setImplementation(new MyDependentServiceImpl())
            .add(createServiceDependency()
                .setService(MyDependencyService.class)
                .setRequired(true))
            .add(createServiceDependency()
                .setService(LogReaderService.class)
                .setCallbacks("addLogReaderService", "removeLogReaderService")
                .setRequired(true))
            .add(createServiceDependency()
                .setService(LogService.class)
                .setRequired(true)));
    }

    /**
     * @see org.apache.felix.dm.DependencyActivatorBase#destroy(org.osgi.framework.BundleContext, org.apache.felix.dm.DependencyManager)
     */
    @Override
    public void destroy(BundleContext context, DependencyManager manager) throws Exception {
        // NO-op
    }
}
