/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.multitenant.conf.configadmin;

import java.util.Dictionary;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import org.amdatu.multitenant.Constants;
import org.amdatu.multitenant.TenantFactoryConfiguration;
import org.osgi.service.cm.ConfigurationException;
import org.osgi.service.cm.ManagedServiceFactory;
import org.osgi.service.log.LogService;

/**
 * Configuration based for tenant factory configurator.
 * 
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 */
public class Configurator implements ManagedServiceFactory, Runnable {

    public static final String PID = "org.amdatu.tenant.factory";
    private static final int DELAY = 2000;

    private final Map<String, Map<String, Object>> m_tenants = new HashMap<String, Map<String, Object>>();
    private final Object m_lock = new Object();

    private volatile TenantFactoryConfiguration m_config;
    private volatile LogService m_log;
    private volatile ScheduledExecutorService m_executor;
    private volatile ScheduledFuture<?> m_future;

    public void start() {
        m_executor = Executors.newSingleThreadScheduledExecutor();
    }

    public void stop() {
        m_executor.shutdown();
        boolean success = false;
        try {
            success = m_executor.awaitTermination(DELAY, TimeUnit.MILLISECONDS);
        }
        catch (InterruptedException e) {
            m_log.log(LogService.LOG_WARNING, "Interrupted while waiting for executor to terminate.");
        }
        finally {
            if (!success) {
                m_executor.shutdownNow();
            }
            m_executor = null;
        }
    }

    @Override
    public void run() {
        Map<String, Map<String, Object>> config;
        synchronized (m_lock) {
            config = toConfig(m_tenants);
        }
        m_config.update(config);
    }

    @Override
    public String getName() {
        return PID;
    }

    @Override
    public void updated(String pid, Dictionary<String, ?> properties) throws ConfigurationException {
        Map<String, Object> map = toMap(properties);
        synchronized (m_lock) {
            m_tenants.put(pid, map);
        }
        scheduleUpdate();
    }

    @Override
    public void deleted(String pid) {
        synchronized (m_lock) {
            m_tenants.remove(pid);
        }
        scheduleUpdate();
    }

    private void scheduleUpdate() {
        if (m_future != null && !m_future.isDone()) {
            m_future.cancel(false);
        }
        m_future = m_executor.schedule(this, DELAY, TimeUnit.MILLISECONDS);
    }

    private static Map<String, Object> toMap(Dictionary<String, ?> properties) throws ConfigurationException {
        Map<String, Object> map = new HashMap<String, Object>(properties.size());
        Enumeration<String> enumeration = properties.keys();
        while (enumeration.hasMoreElements()) {
            String key = enumeration.nextElement();
            map.put(key, properties.get(key));
        }
        if (!map.containsKey(Constants.PID_KEY)) {
            throw new ConfigurationException(Constants.PID_KEY, "missing");
        }
        return map;
    }

    private static Map<String, Map<String, Object>> toConfig(Map<String, Map<String, Object>> tenants) {
        Map<String, Map<String, Object>> map = new HashMap<String, Map<String, Object>>();
        for (Map<String, Object> config : tenants.values()) {
            String tenantPID = (String) config.get(Constants.PID_KEY);
            map.put(tenantPID, new HashMap<String, Object>(config));
        }
        if (!map.containsKey(Constants.PID_VALUE_PLATFORM)) {
            Map<String, Object> props = new HashMap<String, Object>();
            props.put(Constants.NAME_KEY, Constants.NAME_VALUE_PLATFORM);
            map.put(Constants.PID_VALUE_PLATFORM, props);
        }
        return map;
    }
}
