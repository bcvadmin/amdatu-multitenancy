/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.multitenant.demo.eventadmin.observer;

import org.amdatu.multitenant.Tenant;
import org.amdatu.multitenant.TenantLifeCycleListener;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceRegistration;

/**
 * 
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 */
public class ObserverImpl implements TenantLifeCycleListener {

    private ServiceRegistration<?> m_registration;

    public void start(BundleContext context) throws Exception {
        m_registration = context.registerService(TenantLifeCycleListener.class, this, null);
    }

    public void stop(BundleContext context) throws Exception {
        if (m_registration != null) {
            m_registration.unregister();
        }
    }

    @Override
    public void initial(Tenant[] tenants) {
        if (tenants != null) {
            for (Tenant tenant : tenants) {
                if (tenant != null) {
                    System.out.println("Observer - initial: " + tenant.getName() + " " + tenant.getProperties());
                }
            }
        }
    }

    @Override
    public void create(Tenant tenant) {
        if (tenant != null) {
            System.out.println("Observer - create: " + tenant.getName() + " " + tenant.getProperties());
        }
    }

    @Override
    public void update(Tenant tenant) {
        System.out.println("Observer - update: " + tenant.getName() + " " + tenant.getProperties());
    }

    @Override
    public void delete(Tenant tenant) {
        System.out.println("Observer - delete: " + tenant.getName() + " " + tenant.getProperties());
    }
}
