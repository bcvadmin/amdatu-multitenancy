package org.amdatu.multitenant.demo.eventadmin.observer;


import org.apache.felix.dm.DependencyActivatorBase;
import org.apache.felix.dm.DependencyManager;
import org.amdatu.multitenant.TenantLifeCycleListener;
import org.apache.felix.dm.Component;
import org.osgi.framework.BundleContext;

public class Activator extends DependencyActivatorBase {

	@Override
	public void destroy(BundleContext arg0, DependencyManager arg1) throws Exception {

	}

	@Override
	public void init(BundleContext arg0, DependencyManager dm) throws Exception {
		//observer
		Component component = dm.createComponent()
				.setInterface(TenantLifeCycleListener.class.getName(), null)
				.setImplementation(ObserverImpl.class)				
				.setCallbacks(null,"start","stop", null)//init, start, stop and destroy.
				;
		dm.add(component);	
	}	
}