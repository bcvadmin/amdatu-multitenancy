/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.multitenant.demo.eventadmin.handler;

import java.util.concurrent.ScheduledExecutorService;
import org.osgi.framework.BundleContext;
import org.osgi.service.event.Event;
import org.osgi.service.event.EventHandler;

/**
 * Demo consumer component that periodically prints a message containing
 * the name of the available {@code Tenant} and the message returned by
 * the available {@code Producer#getMessage()}.
 * 
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 */
public class EventConsumerImpl implements EventHandler {

    private BundleContext m_context;
    private ScheduledExecutorService m_executor;


	@Override
	public void handleEvent(Event event) {
		System.out.println("Received event...");
	}
}
