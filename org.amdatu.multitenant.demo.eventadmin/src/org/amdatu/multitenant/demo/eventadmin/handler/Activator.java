package org.amdatu.multitenant.demo.eventadmin.handler;


import java.util.Properties;

import org.apache.felix.dm.DependencyActivatorBase;
import org.apache.felix.dm.DependencyManager;
import org.amdatu.multitenant.demo.eventadmin.Constants;
import org.amdatu.multitenant.demo.eventadmin.handler.EventConsumerImpl;
import org.apache.felix.dm.Component;
import org.osgi.framework.BundleContext;
import org.osgi.service.event.EventConstants;
import org.osgi.service.event.EventHandler;

public class Activator extends DependencyActivatorBase {

	@Override
	public void destroy(BundleContext arg0, DependencyManager arg1) throws Exception {

	}

	@Override
	public void init(BundleContext arg0, DependencyManager dm) throws Exception {
		//handler
		Properties properties = new Properties();
		properties.put(EventConstants.EVENT_TOPIC,new String[] {Constants.TOPIC1});
		Component component = dm.createComponent()
				.setInterface(EventHandler.class.getName(), properties)
				.setImplementation(EventConsumerImpl.class)
				;
		dm.add(component);
	}
	
}