/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.multitenant.demo.eventadmin.publisher;

import java.util.Dictionary;
import java.util.Hashtable;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.amdatu.multitenant.Tenant;
import org.amdatu.multitenant.demo.eventadmin.Constants;
import org.apache.felix.dm.Component;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;
import org.osgi.service.event.EventAdmin;

/**
 * Demo provider component that returns the tenant's configured {@code demo.message}
 * 
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 */
public class PublisherImpl implements Publisher, Runnable {

    private volatile BundleContext m_context;
    private EventAdmin m_registration;
	private ScheduledExecutorService m_executor;
	
	public void init(Component component) {
		if (component != null);
	}

    public void start() throws Exception {
        this.m_executor = Executors.newSingleThreadScheduledExecutor();
        this.m_executor.scheduleAtFixedRate(this, 1L, 3L, TimeUnit.SECONDS);
    }

    public void stop(BundleContext context) throws Exception {
        if (m_registration != null) {
        }
    }

	@Override
	public void sendEvent(String topic, Dictionary properties) {
        try {
            ServiceReference<Tenant> tr = m_context.getServiceReference(Tenant.class);
            if (tr != null) {
                Tenant t = m_context.getService(tr);
                if (t != null) {
                    //message = (String) t.getProperties().get("demo.message");
                    m_context.ungetService(tr);
                }
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
	}

    @Override
    public void run() {
        String name = "UNKNOWN";
        try {
            ServiceReference<Tenant> tr = m_context.getServiceReference(Tenant.class);
            if (tr != null) {
                Tenant t = m_context.getService(tr);
                if (t != null) {
                    name = t.getName();
                    m_context.ungetService(tr);
                }
            }
            
            Dictionary<String,?> properties = new Hashtable<>();
            sendEvent(Constants.TOPIC1,properties);

            System.out.println(name + ": " + System.currentTimeMillis());
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
}
