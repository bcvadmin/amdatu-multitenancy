package org.amdatu.multitenant.demo.eventadmin.publisher;


import org.apache.felix.dm.DependencyActivatorBase;
import org.apache.felix.dm.DependencyManager;
import org.amdatu.multitenant.demo.eventadmin.Constants;
import org.amdatu.multitenant.demo.eventadmin.publisher.Publisher;
import org.amdatu.multitenant.demo.eventadmin.publisher.PublisherImpl;
import org.apache.felix.dm.Component;
import org.apache.felix.dm.ComponentState;
import org.apache.felix.dm.ComponentStateListener;
import org.osgi.framework.BundleContext;
import org.osgi.service.event.EventAdmin;

public class Activator extends DependencyActivatorBase {

	@Override
	public void destroy(BundleContext arg0, DependencyManager arg1) throws Exception {

	}

	@Override
	public void init(BundleContext arg0, DependencyManager dm) throws Exception {
		//publisher
		Component component = dm.createComponent()
				.setInterface(Publisher.class.getName(), null)
				.setImplementation(PublisherImpl.class)				
				.setCallbacks("init","start","stop", null)//init, start, stop and destroy.
				.add(createServiceDependency().setService(org.osgi.service.event.EventAdmin.class).setRequired(true).setDebug("PublisherImpl"))
				;
		
		
		component.add(new ComponentStateListener() {
			
			@Override
			public void changed(Component c, ComponentState state) {
				System.out.println(state);
			}
		});
		
		dm.add(component);
	}
	
}