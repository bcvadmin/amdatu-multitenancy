/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.multitenantconf.rp.helper;

import org.apache.ace.client.repository.helper.ArtifactHelper;

/**
 * Provides the constants for the tenant configuration artifact helper.
 * 
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 */
public interface TenantConfHelper extends ArtifactHelper {
    /**
     * Denotes the mimetype used for the specific artifacts.
     */
    public static final String MIMETYPE = "application/vnd.amdatu.tenantconf";
    /**
     * The file extension an artifact should have to be recognized by this helper.
     */
    public static final String EXTENSION = ".tenant";

    /**
     * The key under which the filename of the configuration is stored.
     */
    public static final String KEY_FILENAME = "filename";
    /**
     * The resource processor name.
     */
    public static final String PROCESSOR = "org.amdatu.tenant.conf.rp";
}
