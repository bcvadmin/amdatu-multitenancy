/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.multitenant.factory;

import java.util.Map;

import org.amdatu.multitenant.Constants;
import org.amdatu.multitenant.Tenant;

/**
 * Data-object representing a Tenant.
 * 
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 */
public class TenantService implements Tenant {

    private volatile Map<String, Object> m_properties;

    public TenantService(Map<String, Object> properties) {
        if (properties == null) {
            throw new IllegalArgumentException("Properties cannot be null");
        }
        if (properties.get(Constants.PID_KEY) == null || (!(properties.get(Constants.PID_KEY) instanceof String))
            || properties.get(Constants.PID_KEY).equals("")) {
            throw new IllegalArgumentException("Tenant PID must be valid");
        }
        m_properties = properties;
    }

    public String getPID() {
        return (String) m_properties.get(Constants.PID_KEY);
    }

    public String getName() {
        return (String) m_properties.get(Constants.NAME_KEY);
    }

    @Override
    public Map<String, Object> getProperties() {
        return m_properties;
    }

    public void setProperties(Map<String, Object> properties) {
        if (properties == null) {
            throw new IllegalArgumentException("Properties cannot be null");
        }
        if (properties.get(Constants.PID_KEY) == null || (!(properties.get(Constants.PID_KEY) instanceof String))
            || !properties.get(Constants.PID_KEY).equals(getPID())) {
            throw new IllegalArgumentException("Tenant PID can not change");
        }
        m_properties = properties;
    }

    @Override
    public String toString() {
        return getName() + " (" + getPID() + ")";
    }

    @Override
    public int hashCode() {
        return getPID().hashCode() * 31;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        TenantService other = (TenantService) obj;
        if (getPID() == null && other.getPID() != null) {
            return false;
        }
        if (getPID() != null && other.getPID() == null) {
            return false;
        }
        return getPID().equals(other.getPID());
    }
}
