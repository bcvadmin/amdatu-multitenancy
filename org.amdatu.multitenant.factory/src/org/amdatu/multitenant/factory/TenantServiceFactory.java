/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.multitenant.factory;

import static org.amdatu.multitenant.Constants.LIFECYCLELISTENER_SCOPE_KEY;

import java.util.ArrayList;
import java.util.Dictionary;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.amdatu.multitenant.Constants;
import org.amdatu.multitenant.Tenant;
import org.amdatu.multitenant.TenantFactoryConfiguration;
import org.amdatu.multitenant.TenantLifeCycleListener;
import org.apache.felix.dm.Component;
import org.apache.felix.dm.DependencyManager;
import org.osgi.framework.Filter;
import org.osgi.framework.FrameworkUtil;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.framework.ServiceReference;
import org.osgi.service.log.LogService;

/**
 * Amdatu {@link TenantFactoryConfiguration} implementation responsible for
 * publishing {@link Tenant} services based on configuration provided.
 *
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 *
 */
public final class TenantServiceFactory implements TenantFactoryConfiguration {

    private final Map<String, TenantServiceHolder> m_services = new HashMap<String, TenantServiceHolder>();
    private final Map<ServiceReference<?>, ListenerHolder> m_listeners =
        new HashMap<ServiceReference<?>, ListenerHolder>();

    private volatile DependencyManager m_dependencyManager;
    private volatile Component m_component;
    private volatile LogService m_logService;
    private volatile boolean m_initial = true;

    /*
     * Dependency Manager callback
     */
    public void add(ServiceReference<?> reference, TenantLifeCycleListener listener) {

        try {
            ListenerHolder holder = new ListenerHolder(listener, getScopeFilter(reference));
            Set<Tenant> matches = new HashSet<Tenant>();
            synchronized (m_services) {
                m_listeners.put(reference, holder);
                for (TenantServiceHolder service : m_services.values()) {
                    if (holder.matches(service.getTenant().getProperties())) {
                        matches.add(service.getTenant());
                    }
                }
            }
            holder.initial(matches.toArray(new Tenant[matches.size()]));
        }
        catch (Exception e) {
            m_logService.log(LogService.LOG_WARNING, "Failed to add TenantLifeCycleListener due to exception", e);
        }
    }

    /*
     * Dependency Manager callback
     */
    public void remove(ServiceReference<?> reference, TenantLifeCycleListener listener) {
        synchronized (m_services) {
            m_listeners.remove(reference);
        }
    }

    @Override
    public void update(Map<String, Map<String, Object>> tenants) {

        List<TenantUpdate> createUpdates;
        List<TenantUpdate> updateUpdates;
        List<TenantUpdate> deleteUpdates;
        boolean initial = false;

        // All checks and modifications against local state happen within
        // this synchronized block.
        synchronized (m_services) {
            initial = m_initial && tenants.size() > 0;
            if (initial) {
                m_initial = false;
            }
            deleteUpdates = getDeleteUpdates(tenants);
            updateUpdates = getUpdateUpdates(tenants);
            createUpdates = getCreateUpdates(tenants);
        }

        // All modifications against services and registration happen
        // outside the synchronized block.
        if (deleteUpdates != null) {
            for (TenantUpdate entry : deleteUpdates) {
                entry.run();
            }
        }
        if (updateUpdates != null) {
            for (TenantUpdate entry : updateUpdates) {
                entry.run();
            }
        }
        if (createUpdates != null) {
            for (TenantUpdate entry : createUpdates) {
                entry.run();
            }
        }

        // after receiving our initial, not empty configuration, we register as a listener for
        // tenant life cycle listeners so we can start notifying them
        if (initial) {
            m_component.add(m_dependencyManager.createServiceDependency()
                .setService(TenantLifeCycleListener.class)
                .setCallbacks("add", "remove")
                .setRequired(false));
        }
    }

    /**
     * Returns the scope filter that controls for which tenant events a listener must be called.
     * 
     * @param reference the reference to get the filter from
     * @return a filter condition, may be <code>null</code>.
     * @throws InvalidSyntaxException if the filter does not compile.
     * @throws IllegalArgumentException if the filter if of an unsupported type.
     */
    private static Filter getScopeFilter(ServiceReference<?> reference) throws InvalidSyntaxException {

        // The scope filter can be a string of a filter
        Object scopeFilterProp = reference.getProperty(LIFECYCLELISTENER_SCOPE_KEY);
        Filter scopeFilter = null;
        if (scopeFilterProp != null) {
            if (scopeFilterProp instanceof Filter) {
                scopeFilter = (Filter) scopeFilterProp;
            }
            else if (scopeFilterProp instanceof String) {
                scopeFilter = FrameworkUtil.createFilter((String) scopeFilterProp);
            }
            else {
                throw new IllegalArgumentException("Property " + LIFECYCLELISTENER_SCOPE_KEY
                    + " is of unsupported type " + scopeFilterProp.getClass());
            }
        }
        return scopeFilter;
    }

    /**
     * Returns a copy of a configuration for a single tenant while ensuring that the PID is consistent
     * with the key.
     *
     * @param pid the key
     * @param properties the configuration
     * @return the copy
     */
    private static Map<String, Object> getProperties(String pid, Map<String, Object> properties) {
        Map<String, Object> copy = new HashMap<String, Object>(properties);
        copy.put(Constants.PID_KEY, pid);
        return copy;
    }

    /**
     * Return a copy of a configuration in the form of a {@code Dictionary}. This is to support legacy
     * OSGi and can be remove when this project upgrades to 4.3+.
     * 
     * @param properties the configuration
     * @return the copy
     */
    private static Dictionary<String, Object> toDictionary(Map<String, Object> properties) {
        Dictionary<String, Object> dictionary = new Hashtable<String, Object>();
        for (Entry<String, Object> e : properties.entrySet()) {
            dictionary.put(e.getKey(), e.getValue());
        }
        return dictionary;
    }

    /**
     * Determine whether two configurations are equal by ensuring both have the exact same keys with
     * associated values that or either the same or equal objects.
     * 
     * @param left the left map
     * @param right the right map
     * @return {@code true} if all key/values match, otherwise {@code false}
     */
    private static boolean mapsEqual(Map<String, Object> left, Map<String, Object> right) {
        for (Entry<String, Object> e : left.entrySet()) {
            if (!right.containsKey(e.getKey())
                || (right.get(e.getKey()) != e.getValue()
                && !right.get(e.getKey()).equals(e.getValue()))) {
                return false;
            }
        }
        for (Entry<String, Object> e : right.entrySet()) {
            if (!left.containsKey(e.getKey())
                || (left.get(e.getKey()) != e.getValue()
                && !left.get(e.getKey()).equals(e.getValue()))) {
                return false;
            }
        }
        return true;
    }

    /**
     * Delegate to {@code #update(Map)} for create actions.
     * 
     * @param config the full configuration update
     * @return a list of updates to execute outside sync
     */
    private List<TenantUpdate> getCreateUpdates(Map<String, Map<String, Object>> configs) {

        List<TenantUpdate> updates = null;
        for (Entry<String, Map<String, Object>> entry : configs.entrySet()) {

            if (m_services.containsKey(entry.getKey())) {
                continue;
            }

            Map<String, Object> properties = getProperties(entry.getKey(), entry.getValue());
            TenantService tenant = new TenantService(properties);
            Component component = m_dependencyManager.createComponent()
                .setInterface(Tenant.class.getName(), toDictionary(properties))
                .setImplementation(tenant);
            TenantServiceHolder service = new TenantServiceHolder(component, tenant);
            m_services.put(entry.getKey(), service);

            TenantUpdate update = new CreateUpdate(service, properties);
            for (ListenerHolder holder : m_listeners.values()) {
                boolean matchesNew = holder.matches(properties);
                if (matchesNew) {
                    update.addCreateListener(holder);
                }
            }
            if (updates == null) {
                updates = new ArrayList<TenantUpdate>();
            }
            updates.add(update);
        }
        return updates;
    }

    /**
     * Delegate to {@code #update(Map)} for modified actions.
     * 
     * @param config the full configuration update
     * @return a list of updates to execute outside sync
     */
    private List<TenantUpdate> getUpdateUpdates(Map<String, Map<String, Object>> configs) {

        List<TenantUpdate> updates = null;
        for (Entry<String, TenantServiceHolder> entry : m_services.entrySet()) {
            Map<String, Object> config = configs.get(entry.getKey());
            if (config == null) {
                continue;
            }

            Map<String, Object> properties = getProperties(entry.getKey(), config);
            if (mapsEqual(entry.getValue().getTenant().getProperties(), properties)) {
                continue;
            }

            TenantUpdate update = new UpdateUpdate(entry.getValue(), properties);
            for (ListenerHolder holder : m_listeners.values()) {
                boolean matchesOld = holder.matches(entry.getValue().getTenant().getProperties());
                boolean matchesNew = holder.matches(properties);
                if (!matchesOld && matchesNew) {
                    update.addCreateListener(holder);
                }
                else if (matchesOld && !matchesNew) {
                    update.addDeleteListener(holder);
                }
                else {
                    update.addUpdateListener(holder);
                }
            }
            if (updates == null) {
                updates = new ArrayList<TenantUpdate>();
            }
            updates.add(update);
        }
        return updates;
    }

    /**
     * Delegate to {@code #update(Map)} for update actions.
     * 
     * @param config the full configuration update
     * @return a list of updates to execute outside sync
     */
    private List<TenantUpdate> getDeleteUpdates(Map<String, Map<String, Object>> configs) {

        List<TenantUpdate> updates = null;
        Set<String> removals = null;
        for (Entry<String, TenantServiceHolder> entry : m_services.entrySet()) {
            if (configs.containsKey(entry.getKey())) {
                continue;
            }

            if (removals == null) {
                removals = new HashSet<String>();
            }
            removals.add(entry.getKey());

            TenantUpdate update = new DeleteUpdate(entry.getValue(), null);
            for (ListenerHolder holder : m_listeners.values()) {
                boolean matchesOld = holder.matches(entry.getValue().getTenant().getProperties());
                if (matchesOld) {
                    update.addDeleteListener(holder);
                }
            }
            if (updates == null) {
                updates = new ArrayList<TenantUpdate>();
            }
            updates.add(update);
        }
        if (removals != null) {
            for (String removal : removals) {
                m_services.remove(removal);
            }
        }
        return updates;
    }

    /**
     * Holder class for a registered tenant and it's associated DM component.
     */
    private static class TenantServiceHolder {

        private final Component m_component;
        private final TenantService m_tenant;

        public TenantServiceHolder(Component component, TenantService tenant) {
            m_component = component;
            m_tenant = tenant;
        }

        public Component getComponent() {
            return m_component;
        }

        public TenantService getTenant() {
            return m_tenant;
        }
    }

    /**
     * Holder class for a registered listener and it's associated compiled
     * filter.
     */
    private static class ListenerHolder implements TenantLifeCycleListener {

        private final TenantLifeCycleListener m_listener;
        private final Filter m_filter;

        public ListenerHolder(TenantLifeCycleListener listener, Filter filter) {
            m_listener = listener;
            m_filter = filter;
        }

        public boolean matches(Map<String, Object> props) {
            return m_filter == null || props == null || m_filter.matches(props);
        }

        @Override
        public void initial(Tenant[] tenants) {
            m_listener.initial(tenants);
        }

        @Override
        public void create(Tenant tenant) {
            m_listener.create(tenant);
        }

        @Override
        public void update(Tenant tenant) {
            m_listener.update(tenant);
        }

        @Override
        public void delete(Tenant tenant) {
            m_listener.delete(tenant);
        }
    }

    /**
     * Base class for tenant update actions that need to be executed outside synchronization.
     * See {@code #update(Tenant)} for details.
     */
    private abstract class TenantUpdate {

        private final TenantServiceHolder m_service;
        private final Map<String, Object> m_properties;

        private List<ListenerHolder> m_createListener;
        private List<ListenerHolder> m_updateListener;
        private List<ListenerHolder> m_deleteListener;

        public TenantUpdate(TenantServiceHolder service, Map<String, Object> properties) {
            m_service = service;
            m_properties = properties;
        }

        public TenantServiceHolder getService() {
            return m_service;
        }

        public Map<String, Object> getProperties() {
            return m_properties;
        }

        public void addCreateListener(ListenerHolder listener) {
            if (m_createListener == null) {
                m_createListener = new ArrayList<ListenerHolder>();
            }
            m_createListener.add(listener);
        }

        public void addUpdateListener(ListenerHolder listener) {
            if (m_updateListener == null) {
                m_updateListener = new ArrayList<ListenerHolder>();
            }
            m_updateListener.add(listener);
        }

        public void addDeleteListener(ListenerHolder listener) {
            if (m_deleteListener == null) {
                m_deleteListener = new ArrayList<ListenerHolder>();
            }
            m_deleteListener.add(listener);
        }

        protected void callCreateListeners() {
            if (m_createListener != null) {
                for (ListenerHolder listener : m_createListener) {
                    try {
                        listener.create(m_service.getTenant());
                    }
                    catch (Exception e) {
                        m_logService.log(
                            LogService.LOG_WARNING,
                            "Tenant life cycle listener returned with an exception when invoking create for tenant: "
                                + m_service.getTenant(), e);
                    }
                }
            }
        }

        protected void callUpdateListeners() {
            if (m_updateListener != null) {
                for (ListenerHolder listener : m_updateListener) {
                    try {
                        listener.update(m_service.getTenant());
                    }
                    catch (Exception e) {
                        m_logService.log(
                            LogService.LOG_WARNING,
                            "Tenant life cycle listener returned with an exception when invoking update for tenant: "
                                + m_service.getTenant(), e);
                    }
                }
            }
        }

        protected void callDeleteListeners() {
            if (m_deleteListener != null) {
                for (ListenerHolder listener : m_deleteListener) {
                    try {
                        listener.delete(m_service.getTenant());
                    }
                    catch (Exception e) {
                        m_logService.log(
                            LogService.LOG_WARNING,
                            "Tenant life cycle listener returned with an exception when invoking delete for tenant: "
                                + m_service.getTenant(), e);
                    }
                }
            }
        }

        public abstract void run();
    }

    /**
     * Update class that completes a tenant create.
     */
    private class CreateUpdate extends TenantUpdate {

        public CreateUpdate(TenantServiceHolder service, Map<String, Object> properties) {
            super(service, properties);
        }

        public void run() {
            m_logService.log(LogService.LOG_DEBUG, "Creating tenant: " + getService().getTenant());
            callCreateListeners();
            m_dependencyManager.add(getService().getComponent());
        }
    }

    /**
     * Update class that completes a tenant update.
     */
    private class UpdateUpdate extends TenantUpdate {

        public UpdateUpdate(TenantServiceHolder service, Map<String, Object> properties) {
            super(service, properties);
        }

        public void run() {
            m_logService.log(LogService.LOG_DEBUG, "Modifying tenant: " + getService().getTenant());
            callDeleteListeners();
            callCreateListeners();
            callUpdateListeners();
            getService().getTenant().setProperties(getProperties());
            getService().getComponent().setServiceProperties(toDictionary(getProperties()));
        }
    }

    /**
     * Update class that completes a tenant delete.
     */
    private class DeleteUpdate extends TenantUpdate {

        public DeleteUpdate(TenantServiceHolder service, Map<String, Object> properties) {
            super(service, properties);
        }

        public void run() {
            m_logService.log(LogService.LOG_DEBUG, "Deleting tenant" + getService().getTenant());
            callDeleteListeners();
            m_dependencyManager.remove(getService().getComponent());
        }
    }
}
