/*
 * Copyright (c) 2010-2012 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.multitenant.factory;

import java.util.HashMap;
import java.util.Map;

import junit.framework.Assert;
import junit.framework.TestCase;

import org.amdatu.multitenant.Constants;
import org.amdatu.multitenant.factory.TenantService;

/**
 * Test cases for {@link TenantService}.
 * 
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 */
public class TenantServiceTest extends TestCase {

	/**
	 * Tests that the constructor where a tenant-id is given does not accept null as tenant-id.
	 */

	public void testTenantIdIsMandatory() {
	    Map<String, Object> props = new HashMap<String, Object>();
	    props.put(Constants.NAME_KEY, "foo");
		try {
			new TenantService(props);
			fail();
		} catch (IllegalArgumentException ex) {
			// Expected
		}
	}

	/**
	 * Tests that the constructor with service properties does not accept a null-value.
	 */

	public void testTenantServicePropertiesIsMandatory() {
		try {
			new TenantService(null);
			fail();
		} catch (IllegalArgumentException ex) {
			// Expected
		}
	}

	/**
	 * Tests that the {@link Object#equals(Object)} method for {@link TenantService} only takes the "tenant.id" into consideration. 
	 * See also AMDATU-260.
	 */

	public void testEquality() {
        Map<String, Object> props1 = new HashMap<String, Object>();
        props1.put(Constants.PID_KEY, "t13245");
        props1.put(Constants.NAME_KEY, "Kwik");
		TenantService t1 = new TenantService(props1);

		Map<String, Object> props2 = new HashMap<String, Object>();
        props2.put(Constants.PID_KEY, "t13245");
        props2.put(Constants.NAME_KEY, "Kwek");
		TenantService t2 = new TenantService(props2);
		Assert.assertEquals(t1, t2);

        Map<String, Object> props3 = new HashMap<String, Object>();
        props3.put(Constants.PID_KEY, "t23245");
        props3.put(Constants.NAME_KEY, "Kwek");
		TenantService t3 = new TenantService(props3);
		Assert.assertNotSame(t2, t3);
	}
}
