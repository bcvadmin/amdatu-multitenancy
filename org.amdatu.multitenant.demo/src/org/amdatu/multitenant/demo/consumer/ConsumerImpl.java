/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.multitenant.demo.consumer;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.amdatu.multitenant.Tenant;
import org.amdatu.multitenant.demo.provider.Provider;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;

/**
 * Demo consumer component that periodically prints a message containing
 * the name of the available {@code Tenant} and the message returned by
 * the available {@code Producer#getMessage()}.
 * 
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 */
public class ConsumerImpl implements BundleActivator, Runnable {

    private BundleContext m_context;
    private ScheduledExecutorService m_executor;

    @Override
    public void start(BundleContext context) throws Exception {
        m_context = context;
        m_executor = Executors.newSingleThreadScheduledExecutor();
        m_executor.scheduleAtFixedRate(this, 1, 3, TimeUnit.SECONDS);
    }

    @Override
    public void stop(BundleContext context) throws Exception {
        m_executor.shutdownNow();
    }

    @Override
    public void run() {
        String name = "UNKNOWN";
        String message = "UNKNOWN";
        try {
            ServiceReference<Tenant> tr = m_context.getServiceReference(Tenant.class);
            if (tr != null) {
                Tenant t = m_context.getService(tr);
                if (t != null) {
                    name = t.getName();
                    m_context.ungetService(tr);
                }
            }

            ServiceReference<Provider> pr = m_context.getServiceReference(Provider.class);
            if (pr != null) {
                Provider p = m_context.getService(pr);
                if (p != null) {
                    message = p.getMessage();
                    m_context.ungetService(tr);
                }
            }

            System.out.println(name + ": " + message);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
}
