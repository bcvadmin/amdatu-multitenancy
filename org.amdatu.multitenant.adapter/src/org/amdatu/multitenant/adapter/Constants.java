/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.multitenant.adapter;

/**
 * Compile time constants for multi tenancy adapter.
 * 
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 */
public interface Constants {

    /**
     * Denotes the original bundle activator to use when running a bundle in multi-tenant mode.
     */
    String MULTITENANT_BUNDLE_ACTIVATOR_KEY = "X-MultiTenant-Bundle-Activator";
    /**
     * Denotes the version of the multi-tenant framework.
     */
    String MULTITENANT_VERSION_KEY = "X-MultiTenant-Version";
    String MULTITENANT_VERSION_VALUE = "1";
    /**
     * Denotes the services registered by the tenant-aware bundle as bound to their scope.
     */
    String MULTITENANT_BINDING_KEY = "X-MultiTenant-Binding";
    /** The services will only be available to the platform itself. */
    String MULTITENANT_BINDING_VALUE_PLATFORM = "PLATFORM";
    /** The services will only be available to the individual tenants. */
    String MULTITENANT_BINDING_VALUE_TENANTS = "TENANTS";
    /** The services will be available for both the individual tenants as the platform. */
    String MULTITENANT_BINDING_VALUE_BOTH = "BOTH";
    /**
     * Denotes an optional filter to narrow the tenant binding scope for the tenant-aware bundle
     */
    String MULTITENANT_BINDING_FILTER_KEY = "X-MultiTenant-BindingFilter";

    /** The lookup filter which can narrow the scope of the services seen by tenants. */
    String MULTITENANT_SCOPE_KEY = "X-MultiTenant-Scope";
    /** The placeholder used to replace with a tenant's PID in lookup filters. */
    String TENANT_PID_PLACEHOLDER = "%TENANTPID%";

    /** If not given, the default binding will be tenant-only. */
    String DEFAULT_BINDING = MULTITENANT_BINDING_VALUE_TENANTS;
    /** The default lookup filter that we use for service lookups from tentants. */
    String DEFAULT_SCOPE_FILTER = String.format("(%s=%s)", org.amdatu.multitenant.Constants.PID_KEY,
        TENANT_PID_PLACEHOLDER);

    /** The default lookup filter that we use for the platform-tenant service lookups. */
    String PLATFORM_BINDING_FILTER = String.format("(%s=%s)", org.amdatu.multitenant.Constants.PID_KEY,
        org.amdatu.multitenant.Constants.PID_VALUE_PLATFORM);
    /** The default lookup filter that we use for the tenant-specific service lookups. */
    String TENANTS_BINDING_FILTER = String.format("(!%s)", PLATFORM_BINDING_FILTER);
    /** The default lookup filter that we use for the tenant-specific service lookups. */
    @Deprecated
    String TENANT_BINDING_FILTER = String.format("(!%s)", PLATFORM_BINDING_FILTER);
    String BOTH_BINDING_FILTER = String.format("(%s=%s)", org.amdatu.multitenant.Constants.PID_KEY,
        "*");

    /** Constant for bundle ID as used in the BundleDataStore. */
    String BUNDLE_ID = "bundle.id";
    
    /**
     * Denotes the bundles that should be visible to the tenant-aware bundle 
     */
    String MULTITENANT_BUNDLE_SCOPE_KEY = "X-MultiTenant-BundleScope";
    /** All bundles in visible */
    String MULTITENANT_BUNDLE_SCOPE_FRAMEWORK = "FRAMEWORK";
    /** Only tenant-aware bundles to which the tenant is bound are visible */
    String MULTITENANT_BUNDLE_SCOPE_TENANT = "TENANT";
    
}
