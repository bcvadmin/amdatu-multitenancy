/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.multitenant.adapter;

import static org.amdatu.multitenant.adapter.Constants.BOTH_BINDING_FILTER;
import static org.amdatu.multitenant.adapter.Constants.DEFAULT_BINDING;
import static org.amdatu.multitenant.adapter.Constants.MULTITENANT_BINDING_FILTER_KEY;
import static org.amdatu.multitenant.adapter.Constants.MULTITENANT_BINDING_KEY;
import static org.amdatu.multitenant.adapter.Constants.MULTITENANT_BINDING_VALUE_BOTH;
import static org.amdatu.multitenant.adapter.Constants.MULTITENANT_BINDING_VALUE_PLATFORM;
import static org.amdatu.multitenant.adapter.Constants.MULTITENANT_BINDING_VALUE_TENANTS;
import static org.amdatu.multitenant.adapter.Constants.MULTITENANT_VERSION_KEY;
import static org.amdatu.multitenant.adapter.Constants.MULTITENANT_VERSION_VALUE;
import static org.amdatu.multitenant.adapter.Constants.PLATFORM_BINDING_FILTER;
import static org.amdatu.multitenant.adapter.Constants.TENANTS_BINDING_FILTER;

import org.osgi.framework.Bundle;
import org.osgi.framework.Filter;
import org.osgi.framework.FrameworkUtil;
import org.osgi.framework.InvalidSyntaxException;

public final class Utils {

    private Utils() {
    }

    /**
     * Returns whether or not the bundle has the correct "marker" bundle header
     * ({@link Constants#MULTITENANT_VERSION_KEY MULTITENANT_VERSION_KEY} with {@link Constants#MULTITENANT_VERSION_VALUE MULTITENANT_VERSION_VALUE})
     * denoting it as a valid multi-tenant aware bundle.
     * 
     * @param bundle the bundle to use, cannot be <code>null</code>.
     * @return <code>true</code> if the bundle headers contain the correct "marker" header,
     *         <code>false</code> otherwise.
     */
    public static boolean isMultiTenantAwareBundle(Bundle bundle) {
        String version = (String) bundle.getHeaders().get(MULTITENANT_VERSION_KEY);
        return version != null && MULTITENANT_VERSION_VALUE.equals(version.trim());
    }

    /**
     * Returns whether a string is empty.
     * 
     * @param s the string to test
     * @return {@code true} if it is so
     */
    public static boolean isEmpty(String s) {
        return s == null || "".equals(s);
    }

    /**
     * Returns the join of two (possibly empty) filters.
     * 
     * @param filter1 first filter string
     * @param filter2 second filter string
     * @return join of two filters, possibly {@code null} or empty in case both filters are {@code null} or empty
     */
    public static String joinFilters(String filter1, String filter2) {
        if (isEmpty(filter2)) {
            return filter1;
        }
        if (isEmpty(filter1)) {
            return filter2;
        }
        return String.format("(&%1$s%2$s)", filter1, filter2);
    }

    /**
     * Returns the (default) filter condition that controls the binding of the tenant adapter.
     * 
     * @param bundle the bundle
     * @return a filter condition, never <code>null</code>.
     * @throws InvalidSyntaxException if the (derived) filter does not compile.
     * @throws IllegalArgumentException if the binding is invalid.
     */
    public static Filter getBindingFilter(Bundle bundle) throws InvalidSyntaxException {
        String bindingFilter = (String) bundle.getHeaders().get(MULTITENANT_BINDING_FILTER_KEY);
        String bindingScope = (String) bundle.getHeaders().get(MULTITENANT_BINDING_KEY);
        if (bindingScope == null) {
            bindingScope = DEFAULT_BINDING;
        }
        switch (bindingScope) {
            case MULTITENANT_BINDING_VALUE_PLATFORM:
                bindingFilter = joinFilters(PLATFORM_BINDING_FILTER, bindingFilter);
                break;
            case MULTITENANT_BINDING_VALUE_TENANTS:
                bindingFilter = joinFilters(TENANTS_BINDING_FILTER, bindingFilter);
                break;
            case MULTITENANT_BINDING_VALUE_BOTH:
                bindingFilter = joinFilters(BOTH_BINDING_FILTER, bindingFilter);
                break;
            default:
                throw new IllegalArgumentException("Unsupported binding: " + bindingScope);
        }
        return FrameworkUtil.createFilter(bindingFilter);
    }

}
