/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.fileinstall.tenantconf;

import java.util.Dictionary;
import java.util.Hashtable;

import org.apache.felix.dm.DependencyActivatorBase;
import org.apache.felix.dm.DependencyManager;
import org.apache.felix.fileinstall.ArtifactInstaller;
import org.osgi.framework.BundleContext;
import org.osgi.framework.Constants;
import org.osgi.service.deploymentadmin.spi.ResourceProcessor;
import org.osgi.service.log.LogService;

/**
 * Activator for Apache Felix FileInstall extension that hands of tenantconf configuration artifacts
 * to a tenantconf {@link ResourceProcessor}.
 * 
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 */
public final class Activator extends DependencyActivatorBase {

    /**
     * @see DependencyActivatorBase#init(org.osgi.framework.BundleContext, org.apache.felix.dm.DependencyManager)
     */
    @Override
    public void init(BundleContext context, DependencyManager manager) throws Exception {

        // this prop is just so we can find our service from itest
        Dictionary<String, Object> props = new Hashtable<String, Object>();
        props.put("org.amdatu.tenant.conf.rp", "true");

        manager.add(createComponent()
            .setInterface(ArtifactInstaller.class.getName(), props)
            .setImplementation(TenantConfArtifactInstaller.class)
            .add(
                createServiceDependency()
                    .setService(ResourceProcessor.class,
                        "(" + Constants.SERVICE_PID + "=org.amdatu.tenant.conf.rp)")
                    .setRequired(true))
            .add(createServiceDependency()
                .setService(LogService.class)
                .setRequired(false)));
    }

}
