/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.fileinstall.autoconf;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.felix.fileinstall.ArtifactInstaller;
import org.apache.felix.metatype.MetaData;
import org.apache.felix.metatype.MetaDataReader;
import org.osgi.framework.Bundle;
import org.osgi.service.deploymentadmin.DeploymentPackage;
import org.osgi.service.deploymentadmin.spi.DeploymentSession;
import org.osgi.service.deploymentadmin.spi.ResourceProcessor;
import org.osgi.service.deploymentadmin.spi.ResourceProcessorException;
import org.osgi.service.log.LogService;

/**
 * {@code AutoConfArtifactInstaller} is an extension to Apache Felix FileInstall that hands of
 * metatype configuration artifacts to an autoconf {@link ResourceProcessor}.
 * 
 * This implementation uses a single threaded executor to provide a non blocking implementation
 * that synchronously handles tasks with order guarantee.
 * 
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 */
public final class AutoConfArtifactInstaller implements ArtifactInstaller {

    // injected by Apache Felix DependencyManager
    private volatile ResourceProcessor m_autoConfProcessor;
    private volatile LogService m_logService;

    private ExecutorService m_executorService;

    /**
     * Apache Felix DependencyManager Component lifecycle method.
     * 
     * @throws Exception unexpected error
     */
    public void start() throws Exception {
        m_executorService = Executors.newSingleThreadExecutor();
    }

    /**
     * Apache Felix DependencyManager Component lifecycle method.
     * 
     * @throws Exception unexpected error
     */
    public void stop() throws Exception {
        m_executorService.shutdownNow();
        m_executorService = null;
    }

    /**
     * @see org.apache.felix.fileinstall.ArtifactListener#canHandle(java.io.File)
     */
    public boolean canHandle(File artifact) {
        if (!(artifact.getName().endsWith(".xml"))) {
            return false;
        }
        InputStream stream = null;
        MetaData metaData = null;
        try {
            stream = new BufferedInputStream(new FileInputStream(artifact));
            MetaDataReader reader = new MetaDataReader();
            metaData = reader.parse(stream);
        }
        catch (Exception e) {
            m_logService.log(LogService.LOG_DEBUG, "Unable to parse artifact file " + artifact.getName(), e);
        }
        finally {
            if (stream != null) {
                try {
                    stream.close();
                }
                catch (IOException e) {
                    m_logService.log(LogService.LOG_WARNING,
                        "Unable to close artifact inputstream " + artifact.getName(), e);
                }
            }
        }
        return metaData != null;
    }

    /**
     * @see org.apache.felix.fileinstall.ArtifactInstaller#install(java.io.File)
     */
    public void install(File artifact) throws Exception {
        m_executorService.submit(new ProcessorTransaction(Task.INSTALL, artifact));
    }

    /**
     * @see org.apache.felix.fileinstall.ArtifactInstaller#update(java.io.File)
     */
    public void update(File artifact) throws Exception {
        m_executorService.submit(new ProcessorTransaction(Task.UPDATE, artifact));
    }

    /**
     * @see org.apache.felix.fileinstall.ArtifactInstaller#uninstall(java.io.File)
     */
    public void uninstall(File artifact) throws Exception {
        m_executorService.submit(new ProcessorTransaction(Task.UNINSTALL, artifact));
    }

    static enum Task {
        INSTALL,
            UPDATE,
            UNINSTALL
    }

    // Dummy DeploymentSession
    static class ProcessorSession implements DeploymentSession {

        public DeploymentPackage getTargetDeploymentPackage() {
            return null;
        }

        public DeploymentPackage getSourceDeploymentPackage() {
            return null;
        }

        public File getDataFile(Bundle bundle) {
            return null;
        }
    }

    // Task implementation
    class ProcessorTransaction implements Runnable {

        private final Task m_task;
        private final File m_artifact;

        public ProcessorTransaction(Task task, File artifact) {
            m_task = task;
            m_artifact = artifact;
        }

        public void run() {
            m_logService.log(LogService.LOG_DEBUG,
                "Processing metatype configuration file " + m_artifact.getName() + "(" + m_task.name() + ")");

            InputStream stream = null;
            try {
                m_autoConfProcessor.begin(new ProcessorSession());
                if (m_task == Task.UNINSTALL) {
                    m_autoConfProcessor.dropped(m_artifact.getName());
                }
                else {
                    stream = new BufferedInputStream(new FileInputStream(m_artifact));
                    m_autoConfProcessor.process(m_artifact.getName(), stream);
                }
                m_autoConfProcessor.prepare();
                m_autoConfProcessor.commit();
            }
            catch (FileNotFoundException e) {
                m_logService.log(LogService.LOG_DEBUG,
                    "Processing metatype configuration file " + m_artifact.getName() + "failed", e);
            }
            catch (ResourceProcessorException e) {
                // Thrown by process or dropped
                m_autoConfProcessor.rollback();
                m_logService.log(LogService.LOG_DEBUG,
                    "Processing metatype configuration file " + m_artifact.getName() + "failed", e);
            }
            finally {
                if (stream != null) {
                    try {
                        stream.close();
                    }
                    catch (IOException e) {
                        m_logService.log(LogService.LOG_DEBUG,
                            "Processing metatype configuration file " + m_artifact.getName() + "failed", e);
                    }
                }
            }
        }
    }
}
